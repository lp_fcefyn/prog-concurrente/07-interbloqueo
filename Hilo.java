
public class Hilo implements Runnable{
	private int id;
	public static Object lockA = new Object();
	public static Object lockB = new Object();
	
	public Hilo(int id)
	{
		this.id = id;
	}
	
	@Override
	public void run() 				// Hay veces que el programa se ejecutará correctamente y otras en las que 						
	{								// se generará interbloqueo.
		if(id %2 == 0)	// Hilo	par	// Si ambas colas están ocupadas por los hilos que están activos se tiene interbloqueo.
		{							// Por esto nunca se debe usar un lock dentro de otro.
			synchronized(lockA)		//
			{						//
				mostrarA();			//
			}
		}
		else			//Hilo impar
		{
			synchronized(lockB) 
			{
				mostrarB();
			}
		}
	}
	
	private void mostrarA() 
	{
		synchronized(lockB)
		{
			System.out.println("Hola, soy el hilo "+this.id);
		}
		
	}
	
	private void mostrarB() 
	{
		synchronized(lockA)
		{
			System.out.println("Hola, soy el hilo "+this.id);
		}
	}
}
